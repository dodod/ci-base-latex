FROM ubuntu:focal
MAINTAINER Lukas Mertens <docker@lukas-mertens.de>
ENV DEBIAN_FRONTEND noninteractive
ENV JAVA_TOOL_OPTIONS -Dfile.encoding=UTF8

RUN apt-get update -q && apt-get install -qy \
    texlive texlive-lang-german texlive-latex-extra texlive-science \
		openjdk-8-jdk \
    python-pygments gnuplot \
    make git build-essential \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /data
VOLUME ["/data"]
